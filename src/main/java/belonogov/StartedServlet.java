package belonogov;

import com.mysql.cj.api.Session;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "StartedServlet", urlPatterns = "/")
public class StartedServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher authDS = request.getRequestDispatcher("auth.jsp");
        RequestDispatcher welcomeDS = request.getRequestDispatcher("hello.jsp");
        int count = 0;
        String login = null;


        try {
            Driver driver = (Driver) Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/mydb?useSSL=false&serverTimezone=UTC",
                    "root",
                    "password");
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    String kyka = cookie.getValue();
                    //response.getWriter().write(kyka + " " + login + " " + count + "\n");
                    PreparedStatement ps = connection.prepareStatement("SELECT login FROM users");
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        login = rs.getString("login");

                        if (kyka.equals(login)) {
                            count += 1;
                            request.setAttribute("login", login);
                        }
                    }
                }
            }
            //response.getWriter().write(kyka + " " + login + " " + count + "\n");

            if (count == 1) {
                welcomeDS.forward(request, response);
            } else {
                authDS.forward(request, response);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


//        RequestDispatcher dispatcher = request.getRequestDispatcher("/auth");
//        dispatcher.forward(request, response);


    }
}
