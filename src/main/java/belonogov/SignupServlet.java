package belonogov;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "SignUpServlet", urlPatterns = "/sign-up")
public class SignupServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher helloDispatcher = request.getRequestDispatcher("hello.jsp");
        RequestDispatcher againAuthDispatcher = request.getRequestDispatcher("auth.jsp");
        int count = 0;

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        try {
            Driver driver = (Driver) Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/mydb?useSSL=false&serverTimezone=UTC",
                    "root",
                    "password");

            PreparedStatement ps = connection.prepareStatement(
                    "SELECT login FROM users");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String loginX = rs.getString("login");
                if (login.equals(loginX)) {
                    count += 1;
                }
            }

            if (count == 1) {
                againAuthDispatcher.forward(request, response);
                // если совпадает то обратно на регистрацию
            } else {
                //записать все данные и на welcome его
                PreparedStatement newUser = connection.prepareStatement
                        ("INSERT INTO users(login,pass) VALUES (?,?)");
                newUser.setString(1, login);
                newUser.setString(2, password);
                newUser.executeUpdate();
                Cookie cookie = new Cookie("login", login);
                cookie.setMaxAge(60 * 60 * 24);
                response.addCookie(cookie);
                helloDispatcher.forward(request, response);

            }
//            while (rs.next()) {
//                String loginX = rs.getString("login");
//                response.getWriter().write(login + " " + loginX + "\n");
//                if (login.equals(loginX)) {
//                    Cookie cookie = new Cookie("login", login);
//                    cookie.setMaxAge(60 * 60 * 24);
//                    response.addCookie(cookie);
//                    response.getWriter().write("Have equals" + "\n");
//                    //againAuthDispatcher.forward(request, response);
//                    // если совпадает то обратно на регистрацию
////                }
//                }
//            }
//
//
//            //записать все данные и на welcome его
//            response.getWriter().write("Not equals" + "\n");
//            PreparedStatement newUser = connection.prepareStatement
//                    ("INSERT INTO users(login,pass) VALUES (?,?)");
//            newUser.setString(1, login);
//            newUser.setString(2, password);
//            newUser.executeUpdate();
//            //helloDispatcher.forward(request, response);
//            response.getWriter().write("ended pole");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("sign-up.jsp");
        dispatcher.forward(request, response);
    }
}
