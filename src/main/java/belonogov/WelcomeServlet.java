package belonogov;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;
import java.util.Enumeration;

@WebServlet(name = "WelcomeServlet", urlPatterns = "/welcome")
public class WelcomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher helloDispatchet = request.getRequestDispatcher("hello.jsp");
        RequestDispatcher againAuthDispatchet = request.getRequestDispatcher("auth.jsp");
        int count=0;
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        try {
            Driver driver = (Driver) Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/mydb?useSSL=false&serverTimezone=UTC",
                    "root",
                    "password");
            PreparedStatement ps = connection.prepareStatement("SELECT login , pass FROM users WHERE login=? AND pass=?");
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String loginX = rs.getString("login");
                String passwordX = rs.getString("pass");

                if (login.equals(loginX) && password.equals(passwordX)) {
                    count+=1;
                }
            }
            if (count==1){
                Cookie cookie = new Cookie("login", login);
                cookie.setMaxAge(60*60*24);
                response.addCookie(cookie);
                helloDispatchet.forward(request, response);
            } else {
                againAuthDispatchet.forward(request, response);
            }




        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

