<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>  <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <title>Title</title>
</head>
<body>
<div style="width:1000px; margin:0 auto" >
<h4><script language="JavaScript">
    statusY = document.getElementById("statusY");
    day = new Date();
    hour = day.getHours();
    if (hour>=6 && hour<10)
        greeting = "Good morning,";
    else {
        if (hour>=10 && hour<18)
            greeting = "Good afternoon,";
        else {
            if (hour>=18 && hour<22)
                greeting = "Good evening,";
            else {
                if (hour>=0 && hour<6 || hour>=22 && hour<24)
                    greeting = "Goodnight,"; }
        }
    }
    document.write(greeting)
</script>
${param.login} ${login} !</h4>

<form action="/exit" method="get">
    <input type="submit" class="btn btn-primary" role="button" value="Exit"/>
</form>
</div>
</body>
</html>
