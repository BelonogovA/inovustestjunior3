<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <title>Auth</title>
    <script>
        function checkForm(form) {
            var statusX = document.getElementById("statusX");
            for (var i = 0; i < form.elements.length; i++)
                if (form.elements[i].value == '') {
                    statusX.innerHTML = "Необходимо ввести учетные данные";
                    return false;
                }
        }
    </script>
</head>
<body>
<div style="width:1000px; margin:0 auto">
    <form action="/welcome" method="post" onsubmit="return checkForm(this)">
        <h1>Welcome</h1>
        <table>
            <tr>
                <td>Имя пользователя:</td>
                <td><input name="login" type="text" class="form-control"></td>
                <td><span id="statusX"></span></td>
            </tr>

            <tr>
                <td>Пароль:</td>
                <td><input name="password" type="password" class="form-control"></td>
            </tr>
        </table>

        <tr>
            <td></td>
            <td></td>
        </tr>
        <input type="submit" class="btn btn-primary" role="button" value="Войти"/>

    </form>

    <form action="/sign-up">
        <input type="submit" class="btn btn-primary" role="button" value="Регистрация"/>
    </form>
</div>
</body>
</html>


<%--function validateLogin() {--%>
<%--var x = document.getElementsByName("login").value;--%>
<%--var statusX = document.getElementById("statusX");--%>
<%--if (isEmpty(x)) {--%>
<%--statusX.innerHTML = "Поле пустое";--%>
<%--}--%>
<%--}--%>
<%--function isEmpty(x) {--%>
<%--return (x == null) || (x.length == 0);--%>
<%--}--%>