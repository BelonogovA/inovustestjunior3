<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <title>Sign-up</title>

    <script>
        function checkForm(form) {
            var statusX = document.getElementById("statusX");

            var pass1 = document.getElementById("password").value;
            var pass2 = document.getElementById("repPassword").value;
            var login = document.getElementById("login").value;

            if (pass1 != pass2) {
                statusX.innerHTML = "Пароль и повтор пароля не совпадают";
                return false;
            }

            for (var i = 0; i < form.elements.length; i++)
                if (form.elements[i].value == '') {
                    statusX.innerHTML = "Необходимо ввести учетные данные";
                    return false;
                }

        }
    </script>
</head>
<body>
<div style="width: 700px;">
    <form action="/sign-up" method="post" onsubmit="return checkForm(this)">
        <table>

            <tr>
                <td align="right">Имя пользователя:</td>
                <td><input name="login"
                           type="text"
                           id="login"
                           pattern="(?=^.{4,}$)(?=.*[0-9])(?=.*[A-Za-z]).*"
                           placeholder="Login"
                           class="form-control"
                           title="Имя пользователя должно быть длинее 4 символов и состоять из цифр и букв английского алфавита">
                </td>

            </tr>


            <tr>
                <td align="right">Пароль:</td>
                <td><input name="password"
                           type="password"
                           id="password"
                           placeholder="Password"
                           class="form-control"
                           pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*"
                           title="Пароль недостаточно сложен: должны быть цифры, заглавные и строчные буквы и длинна минимум 8 символов">
                </td>
                <td></td>
            </tr>

            <tr>
                <td align="right">Повтор пароля:</td>
                <td><input name="repPassword"
                           type="password"
                           id="repPassword"
                           class="form-control"
                           placeholder="Repeat password"
                >
                </td>
                <td></td>
            </tr>

            <tr>
                <td align="right" width="200">
                    <input type="submit" class="btn btn-primary" role="button" value="Войти"/>
                </td>
            </tr>
            <tr><td></td>

            </tr>

        </table>

    </form>
</div>
</body>
</html>
